<?php

/**
 * @file
 * Layout mangaer theme file.
 */

/**
 * Theme admin/build/layout_manager
 */
function theme_layout_manager_overview($variables) {
  $form = $variables['form'];
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['title'])) {
      $template = &$form[$key];

      $row = array();
      $row[] = $template['thumbnail'];
      $row[] = $template['title'];
      $row[] = $template['description'];
      $row[] = $template['edit'];

      $rows[] = $row; //array('data' => $row);
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No layouts available.'), 'colspan' => '5'));
  }

  $header = array(t('Thumbnail'), t('Title'), t('Description'), t('Operations'));

  return theme('table', array('header' => $header, 'rows' => $rows)) . drupal_render($form);
}

function theme_layout_manager_content_type_defaults($form) {
  $rows = array();
  foreach (element_children($form) as $key) {
    if (isset($form[$key]['title'])) {
      $template = &$form[$key];
      $row = array();
      $row[] = $template['title']["#value"];
      $row[] = $template['default']["#value"];
      $row[] = $template['edit']["#value"];
      $row[] = $template['delete']["#value"];
      $rows[] = $row; //array('data' => $row);
    }
  }
  if (empty($rows)) {
    $rows[] = array(array('data' => t('No layouts available.'), 'colspan' => '5'));
  }

  $header = array(t('Title'), t('Default Layout'));
  $header[] = array('data' => t('Operations'), 'colspan' => '2');
  return theme('table', array('header' => $header, 'rows' => $rows));
}

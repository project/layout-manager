
/* 
 * -- Blocks Module --
 */
	
var LayoutsBlocks = {

	beanInterfaceURL: function(beanID, beanType) {
		return '/admin/structure/layout_manager/bean_form/' + beanType + '/' + beanID;
	},

	// Assign the region the block is in on sort or drop
	identifyBlockInstance: function(event, ui) {
		jQuery('#layoutsStage .layoutsBlockWidget').each(function(i) {
			var regionID = jQuery(this).parent().attr('id');
			jQuery(this).attr('data-region', regionID);
		});
	},
	
	// If the instance-id is blank, this is a new bean instance on the stage. Let's assign it a unique instance id
	identifyBeanInstances: function(event, ui) {
		Drupal.attachBehaviors();
	},

	setupBlockTools: function() {
		jQuery('#layoutsStage').find('.region').find('.delete').live('click', function(e) {
			e.preventDefault();
			var remove = confirm("Are you sure you want to delete this block from the page?");
			if (remove) {
				jQuery(this).parent().parent().parent().remove();
			}
		});
	},
	
	initialize: function() {
		LayoutsBlocks.setupBlockTools();
	}

}

jQuery(document).ready(function() {

	LayoutsBlocks.initialize();

	/*
		TODO This needs to be cleaned up
	*/

	var $layoutsBlocks = jQuery('#layoutsBlockItems');

	// Monitor the textfield for filtering
	jQuery('#layoutsFilterWidgets').keyup(function() {
		if (jQuery(this).val() === '')
		{
			$layoutsBlocks.find('.layoutsBlockWidget').show();
			return;
		}
		var theItems = $layoutsBlocks.find('.layoutsBlockWidget:Contains(' + jQuery(this).val() + ')');
		$layoutsBlocks.find('.layoutsBlockWidget').hide();
		theItems.show();
	});

});

/* 
 * -- Chunks Module --
 */
(function() {
	
	var Chunks = {
		
		layoutChunks: [],
		layoutElements: [],
		extraChunkIndex: 0,
		
		/**
		 * Highlights the corresponding layout chunk thumbnail
		 * @param {Number} index The array index (or object id from layoutChunks) 
		 */
		setActiveThumbnail: function(index)
		{
			jQuery(Chunks.layoutElements).each(function() { jQuery(this).removeClass('active'); });
			jQuery(Chunks.layoutElements[index]).addClass('active');
		},
		
		/**
		 * Reset the extra chunks thumbnails 
		 */
		resetExtraThumbnails: function($layoutsExtraChunks)
		{
			jQuery($layoutsExtraChunks).each(function() { jQuery(this).removeClass('active'); });
		},
		
		/**
		 * Applies the drag events on new stuff 
		 */
		setupDraggables: function() 
		{
			// If there is more than one layout chunk present, let's allow them to be sortable and removable
			var $layoutChunks = jQuery('#layoutsStage').find('.layout');
			if ($layoutChunks.length > 1)
			{
				$layoutChunks.each(function(i) {
					// If there isn't a drag handle present, let's add one
					if(jQuery(this).find('.layoutsDragHandle').length == 0)
					{
						jQuery(this).append('<span class="layoutsDragHandle"><i class="draggable"></i></span>');
						jQuery(this).append('<span class="layoutsDeleteIcon"><i class="delete"></i></span>');
					}
				});
				// Let's reapply the sortable event
				jQuery('#layoutsStage').sortable();
				jQuery('#layoutsStage').find('.layoutsDeleteIcon').click(function(e) {
					var remove = confirm("Are you sure you want to delete this chunk from the page?");
					if (remove) { jQuery(this).parent().remove(); }
				});
			}
			jQuery('#layoutsStage').find('.region').sortable({ connectWith: '.region', stop: LayoutsBlocks.identifyBlockInstance });
			jQuery('#layoutsBlockItems').find('.layoutsBlockWidget').draggable({ connectToSortable: '.region', revert: 'invalid', helper: 'clone', containment: 'window', scroll: false, stop: LayoutsBlocks.identifyBeanInstances });
		},

		/**
		 * Set the layout id
		 */
		saveID: function(id) {
			var layout = jQuery('#layoutsStage .layout').last();
			log('id is ' + id);
			layout.data('id', id);
		},
		
		/**
		 * Loads the chunkObject's path via Ajax into the stage
		 * Calls setActiveThumbnail when the path is successfully loaded
		 * @param {Object} chunkObject The object of the clicked layoutChunk 
		 * @param {Boolean} append If true, it will append the layout instead of replacing it
		 */
		load: function(chunkObject, append)
		{
			jQuery.ajax({
				url: chunkObject.path,
				type: 'GET',
				success: function(data) {
					if (append === true) {
						jQuery('#layoutsStage').append(data.data);
						jQuery('#layoutsExtraChunks').hide();
					} else {
						jQuery('#layoutsStage').html(data.data);
					}
					Chunks.saveID(chunkObject.lid);
					// Re-apply the draggable events
					Chunks.setupDraggables();
					Chunks.setActiveThumbnail(chunkObject.id);
					log('Layouts: Loaded chunk "' + chunkObject.path + '"');
				}
				
			});
		},
		
		/**
		 * Sets the loading and reset events on the layout chunk menu 
		 */
		initializeEvents: function() {
			// Remove the active state from the layout chunks
			// Set the active chunk class
			// Load the chunk's path into the stage
			jQuery(Chunks.layoutChunks).each(function(i) {
				this.element.click(function(e) {
					Chunks.load(Chunks.layoutChunks[i]);
				});
			});
			
			var $layoutsExtraChunks = jQuery('#layoutsExtraChunks').find('li');
			
			jQuery('#layoutsShowExtraChunks').click(function(e) {
				e.preventDefault();
				Chunks.resetExtraThumbnails($layoutsExtraChunks);
				jQuery('#layoutsExtraChunks').show();
			});
			
			$layoutsExtraChunks.each(function(i) {
				jQuery(this).click(function(e) {
					Chunks.resetExtraThumbnails($layoutsExtraChunks);
					Chunks.extraChunkIndex = i;
					jQuery(this).addClass('active');
				});
			});
			
			jQuery('#layoutsAppendButton').click(function(e) {
				Chunks.load(Chunks.layoutChunks[Chunks.extraChunkIndex], true);
			});
			
		},
		
		/**
		 * Initialize the layout chunks 
		 */
		initialize: function()
		{
			Chunks.layoutElements = $layoutChunks = jQuery('#layoutsChunks').find('li');
			$layoutChunks.each(function(i) {
				Chunks.layoutChunks.push({ id:i, element:jQuery(this), path:jQuery(this).data('path'), lid:jQuery(this).data('id') });
			});
			log('Layouts: Loaded ' + Chunks.layoutChunks.length + ' elements');
			
			Chunks.initializeEvents();
			Chunks.setupDraggables();
			
			// Load the default layout chunk
			// TODO select the active chunk to load
			// Chunks.load(Chunks.layoutChunks[0]);

		}
		
	}
	
	jQuery(document).ready(function() {
		Chunks.initialize();
	});
	
})();
// usage: log('inside coolFunc',this,arguments);
window.log = function() {
	log.history = log.history || [];   // store logs to an array for reference
	log.history.push(arguments);
	if (this.console)
	{
	  console.log( Array.prototype.slice.call(arguments) );
	}
}

// Make jQuery's contains case insensitive
jQuery.expr[':'].Contains = function(a, i, m) {
  return jQuery(a).text().toUpperCase().indexOf(m[3].toUpperCase()) >= 0;
};
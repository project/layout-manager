(function() {
	
	var Layouts = {
		
		callbackURL: jQuery("#layoutsEditor").data('url'),
		
		initialize: function() {
			
			$layoutsStage = jQuery('#layoutsStage');

			jQuery('#layout-manager-form-buttons').submit(function() {

				var layoutStr = '';
				var regionStr = '';
				var blocksStr = '';

				$layouts = jQuery($layoutsStage).find('.layout');

				for (var i=0; i < $layouts.length; i++) {

					// For each REGION in each LAYOUT
					var regions = jQuery($layouts[i]).find('.region');
					for (var j=0; j < regions.length; j++) {
						
						// for each BLOCK in the LAYOUTS REGIONS
						var blocks = jQuery(regions[j]).find('.layoutsBlockWidget');
						
						for (var z=0; z < blocks.length; z++) {
							if (jQuery(blocks[z]).attr('data-instance') != undefined) {
								blocksStr += '"' + z + '": ' + '"' + jQuery(blocks[z]).attr('data-instance') + '"';
							} else {
								blocksStr += '"' + z + '": ' + '"' + jQuery(blocks[z]).attr('data-id') + '"';
							}
							if (z != blocks.length - 1) { blocksStr += ', ' }
						}
						
						regionStr += '"region' + j + '": {'
								+ '"id": ' + '"' + jQuery(regions[j]).attr('id') + '",'
								+ '"blocks": {' + blocksStr + '}'
								+ '}';
						if (j != regions.length - 1) { regionStr += ', ' }
						
						// Reset the blocksStr
						blocksStr = '';
					}

					// Create the layout_manager object
					var layoutID = 'layout' + i;
					layoutStr += '"' + layoutID + '"' + ': ' + '{'
							  + '"id": "' + jQuery($layouts[i]).data('id') + '",'
							  + '"title": "' + jQuery($layouts[i]).attr('id') + '", '
							  + '"regions": ' + '{'
							  + regionStr
							  + '}}';
					if (i != $layouts.length - 1) { layoutStr += ', ' }
					
					// Reset the regionStr
					regionStr = '';
					
				}
				
				jQuery('#json-store').val('{' + layoutStr + '}');
				
				return true;
				
			});

		}
	}
	
	jQuery(document).ready(function() {
		Layouts.initialize();
	});

	Drupal.behaviors.layoutsModule = {
		attach: function(context, settings) {
			jQuery('.layoutsBlockWidget .settings', context).once('layout_manager-settings-button', function() {
				var layoutWidget = jQuery(this).parents('.layoutsBlockWidget');
				
				var beanType = jQuery(layoutWidget).attr('data-type');
				var beanId = jQuery(layoutWidget).attr('data-instance');
				if (beanId == 'new') {
					var date = new Date();
					beanId = (date.getMilliseconds().toString()) + (Math.floor(Math.random() * 1000) + 2);
					jQuery(layoutWidget).attr('data-instance', beanId);
				}
				this.href='/admin/structure/layout_manager/bean_form/' + beanType + '/' + beanId + '/nojs';
				jQuery(this).addClass('ctools-use-modal');
			});
		}
	}
})();

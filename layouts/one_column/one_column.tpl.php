<?php $layoutName = "oneColumn"; ?>

<div id="<?php echo $layoutName; ?>" class="layout group">
  <?php if (isset($vars)) { print '<h2>'.$vars['title'].'</h2>'; } ?>
		<?php if (isset($tabs2)): print '<ul class="tabs secondary">'. $tabs2 .'</ul>'; endif; ?>

	<div id="<?php echo $layoutName.'-1'; ?>" class="region" style="float:left; width:69%; ">
    <?php
      $region = $layoutName.'-1';
      if (isset($$region)) {
        echo $$region;
      }
    ?>
	</div>

	<div id="<?php echo $layoutName.'-2'; ?>" class="region" style="float:right; width:29%; ">
    <?php
      $region = $layoutName.'-2';
      if (isset($$region)) {
        echo $$region;
      }
    ?>
	</div>
	
</div>

<?php $layoutName = "twoHalfColumn"; ?>

<div id="<?php echo $layoutName; ?>" class="layout group">
	<div class="group">
		<div id="<?php echo $layoutName.'-1'; ?>" class="region" style="width: 49%; float: left;">
      <?php
        $region = $layoutName.'-1';
        if (isset($$region)) {
          echo $$region;
        }
      ?>		</div>
		<div id="<?php echo $layoutName.'-2'; ?>" class="region" style="width: 49%; float: right;">
      <?php
        $region = $layoutName.'-2';
        if (isset($$region)) {
          echo $$region;
        }
      ?>		</div>
	</div>
	<!-- /group -->
</div>
<!-- /contentcontainer -->
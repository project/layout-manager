	<section id="layoutsMain" role="contentinfo">

		<div id="layoutsEditor" class="column-main" data-url="<?php if (isset($save_callback)) print $save_callback; ?>">
			<div class="column-wrapper clearfix">

				<div id="layoutsChunks" role="widget">

					<ul class="group">
	          <?php
	            if ($layouts) {
	              foreach ($layouts as $l) {
	                // TODO fix this - we probably don't have an active template when they're stacked
	                $active = ''; // ($l->id == $layout->id) ? 'class="active"' : '';
					print '<li '. $active .' data-id="'.$l->id.'" data-path="'.$l->path.'"><img src="'.$l->thumbnail_image.'" width="38" height="25" alt="'.$l->title.'"></li>';
	              }
	            }
	          ?>
					</ul>
				</div>
				<!-- /layoutsChunks -->

				<div id="layoutsStage">
					<div id="layoutsModal"></div>

		        <?php
		          if (isset($rendered_layout)) {
		            print $rendered_layout;
		          }
		        ?>
				</div>
				<!-- /layoutsStage -->

				<div id="layoutsChunkInsert">
					<button id="layoutsShowExtraChunks" class="teaser-button">Insert Section</button>
					<span class="instruction">&ndash; Add another section to this page &ndash;</span>
					<div id="layoutsExtraChunks" class="group">
						<h1>Select a layout:</h1>
						<ul class="left group">
		            <?php
		            if ($layouts) {
		              foreach ($layouts as $l) {
		                // TODO fix active
		                $active = ''; //($l->id == $layout->id) ? 'class="active"' : '';
		                print '<li '. $active .' data-id="'.$l->id.'" data-path="'.$l->path.'"><img src="'.$l->thumbnail_image.'" width="38" height="25" alt="'.$l->title.'"></li>';
		              }
		            }
		            ?>
						</ul>
						<button id="layoutsAppendButton" class="teaser-button left">Insert Section</button>
					</div>
				</div>
				<!-- /layoutsChunkInsert -->
			</div>
			<!-- /column-wrapper -->
		</div>
		<!-- /column-main -->
        
		<div class="column-side"> 
			<div class="column-wrapper clearfix">
				<?php if ($nid) { ?>
				<table>
                    <tr>
                      <th width="130"><label for="default_layout">Using Default Layout:</label></th>
                      <td>
                        <input type="checkbox" id="default_layout" disabled="disabled" <?php if ($default) { ?>checked="checked" <?php } ?>/>
                      </td>
                      <?php if (!$default) { ?><td>To re-enable default: reset layout</td> <?php } ?>
                    </tr>
                </table>
                <?php } ?>
                
				<?php print $form_buttons ?>

				<div id="layouts" role="widget">
					<div id="layoutsFilterBlocks">
						<input type="text" name="filter_widgets" placeholder="Filter blocks..." value="" id="layoutsFilterWidgets">
					</div>
					<div id="layoutsBlockItems">
	          <?php foreach ($blocks as $block) { ?>
		
            <div data-id="<?php echo $block['bid']; ?>" class="layoutsBlockWidget">
              <div class="toolbar">
                <h1><?php echo $block['title']; ?></h1>
                <div class="tools">
                  <a class="ir delete" href="#" title="Remove this Block">Remove Block</a>
                </div>
              </div>
              <p>Do we have access to the description</p>
            </div>

	            <?php }
                if (isset($beans)) {
                  foreach ($beans as $block) {
              ?>
                  <div data-instance="new" data-type="<?php echo $block['bean']; ?>" class="layoutsBlockWidget">
                   <div class="toolbar">
                     <h1><?php echo $block['name']; ?></h1>
                     <div class="tools">
                       <a class="ir settings" href="#" title="Block Settings">Settings</a>
                       <a class="ir delete" href="#" title="Remove this Block">Remove Block</a>
                     </div>
                   </div>
                   <p>Do we have access to the description</p>
                 </div>

              <?php
                  }
                }
              ?>
					</div>
				</div>
				<!-- /layouts -->
			</div>
			<!-- /column-wrapper -->
		</div>
		<!-- /column-side -->
	</section>

<?php
  if (isset($scripts)) {
    print $scripts;
  }
?>



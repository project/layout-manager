<?php

/**
 * @file
 * Layout mangaer admin file.
 */

/**
 *
 * @return string
 */
function layout_manager_overview() {
  $query = 'SELECT id, thumbnail_image, title, description, default_layout
				 FROM {layout_manager_layouts}';
  $layouts = db_query($query);

  // Initialize form array, then populate with layout_manager from db
  $form = array();
  foreach ($layouts as $layout) {
    $form[$layout->id]['thumbnail'] = "<img src='$layout->thumbnail_image'/>";
    $form[$layout->id]['title'] = check_plain($layout->title);
    $form[$layout->id]['description'] = check_plain($layout->description);
    $form[$layout->id]['edit'] = l(t('edit'), "admin/structure/layout_manager/$layout->id/edit");
  }
  return theme('layout_manager_overview', array('form' => $form));
}

function layout_manager_layout_create_form($form, &$form_state, $layout = NULL) {
  if ($layout) {
    $query = 'SELECT * FROM {layout_manager_layouts}
					 WHERE id = :id';
    $result = db_query($query, array(':id' => $layout));
    $layout = $result->fetchObject();
  }

  $form['layout'] = array(
    '#type' => 'fieldset',
    '#title' => t('Layout Info'),
    '#collapsible' => TRUE,
  );

  $form['layout']['title'] = array('#type' => 'textfield',
    '#title' => t('Layout Title'),
    '#default_value' => isset($layout) ? $layout->title : '',
    '#maxlength' => 255,
    '#description' => t('The name for this url, e.g., <em>"Wireless Home Page"</em>.'),
    '#required' => TRUE,
  );
  $form['layout']['description'] = array('#type' => 'textarea',
    '#title' => t('Description'),
    '#default_value' => isset($layout) ? $layout->description : '',
    '#description' => t('Description of the topic.'),
  );
  $form['layout']['file_name'] = array('#type' => 'textfield',
    '#title' => t('File Name'),
    '#default_value' => isset($layout) ? $layout->file_name : '',
    '#maxlength' => 255,
    '#description' => t('The full filename of the layout file to render.'),
    '#required' => TRUE,
  );
  $form['layout']['thumbnail_image'] = array('#type' => 'textfield',
    '#title' => t('Thumbnail Image'),
    '#default_value' => isset($layout) ? $layout->thumbnail_image : '',
    '#maxlength' => 255,
    '#description' => t('The thumbnail image of the layout for use in the layout builder.'),
    '#required' => TRUE,
  );
  $form['layout']['default_layout'] = array('#type' => 'checkbox',
    '#title' => t('Default layout'),
    '#default_value' => isset($layout) ? $layout->default_layout : '',
    '#description' => t('Indicates whether this layout is displayed for all content types where a layout has not been specified.')
  );
  $form['layout']['regions'] = array('#type' => 'textarea',
    '#title' => t('Block Regions'),
    '#default_value' => isset($layout) ? $layout->regions : '',
    '#description' => t('Enter in each block region that this layout supports, one per line.'),
  );
  $options = array();

  $unused_types = _layout_manager_get_types_as_options(array_keys(node_type_get_types()));
  if ($layout)
    $default_values = _layout_manager_content_types_for_select($layout->id);
  $form['layout']['form_content_types'] = array('#type' => 'select',
    '#title' => 'Display for content type',
    '#default_value' => isset($layout) ? $default_values : '',
    '#options' => $unused_types,
    '#description' => 'Choose content topics this page is related to.',
    '#multiple' => TRUE,
  );
  $form['layout']['use_all_blocks'] = array(
    '#type' => 'checkbox',
    '#title' => t('Use All Blocks'),
    '#description' => t('Select to use all blocks in this layout. Allowed blocks list below will be ignored.'),
    '#default_value' => isset($layout) ? $layout->use_all_blocks : '',
  );
  if ($layout)
    $default_values = _layout_manager_allowed_blocks_for_select($layout->id);
  $form['layout']['form_allowed_blocks'] = array('#type' => 'select',
    '#title' => 'Allowed Blocks',
    '#default_value' => isset($layout) ? $default_values : '',
    '#options' => _layout_manager_get_all_blocks_as_form_options(),
    '#description' => 'Choose content blocks available for this layout.',
    '#multiple' => TRUE,
  );

  $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
  if (isset($layout->id)) {
    $form['id'] = array('#type' => 'value', '#value' => $layout->id);
    // TODO confirm this can be removed
    //	$form['module'] = array('#type' => 'value', '#value' => $layout->module_name);
  }

  return $form;
}

function layout_manager_content_type_defaults() {
  $content_types = node_type_get_types();
  $form = array('#tree' => TRUE);

  $query = 'SELECT *
                 FROM {layout_manager_layout_defaults}';
  $result = db_query($query);
  $defaults = array();
  foreach ($result as $default) {
    $defaults[$default->content_type] = $default->lid;
  }

  $layout_query = 'SELECT id, title
                         FROM {layout_manager_layouts}';
  $layout_result = db_query($layout_query);
  $layouts = array();
  foreach ($layout_result as $layout) {
    $layouts[$layout->id] = $layout->title;
  }

  foreach ($content_types as $type) {
    isset($defaults[$type->type]) ? $title = $layouts[$defaults[$type->type]] : $title = 'none';
    $form[$type->type]['title'] = array('#value' => $type->type);
    $form[$type->type]['default'] = array('#value' => $title);
    $form[$type->type]['edit'] = array('#value' => l(t('set default'), "admin/structure/layout_manager/defaults/" . $type->type));
    $form[$type->type]['delete'] = array('#value' => l(t('remove default'), "admin/structure/layout_manager/remove_default/" . $type->type));
  }

  return theme('layout_manager_content_type_defaults', $form);
}

function layout_manager_set_default_layout($form_state, $type) {
  /** NOT BEING USED 

    $form = array();
    $form['regions'] = array(
    '#prefix' => '<br/><h3>Default layout for: '.$type.'</h3><p>Choose the layout and blocks you want to display for nodes of this content type.</p>',
    '#type' => 'fieldset',
    '#title' => t('Regions'),
    '#tree' => TRUE,
    '#collapsible' => TRUE,
    );
    $form['regions']['fields'] = array(
    '#type' => 'layouts_builder_field',
    '#options' => _layouts_get_all_blocks_as_options(),
    '#element' => 'testyo',
    '#node' => null,
    '#default_content_type' => $type,
    );
    $form['submit'] = array('#type' => 'submit', '#value' => t('Save'));
    $form['content_type'] = array('#type' => 'value', '#value' => $type);
    return $form;

   * */
}

function layout_manager_remove_layout($content_type) {
  $sql = db_delete('layout_manager_layout_defaults')->condition('content_type', $content_type);
  $sql->execute();

  drupal_set_message(t('Removed default layout for %name.', array('%name' => $content_type)));

  drupal_goto('admin/structure/layout_manager/defaults');
}

/* ===================== LAYOUTS FORM SUBMITS ====================== */

function layout_manager_layout_create_form_submit($form, &$form_state) {
  $test = 'test';
  if (isset($form_state['values']['id'])) {
    $query = 'UPDATE {layout_manager_layouts}
					 SET title = ?, description = ?, file_name = ?, thumbnail_image = ?, default_layout = ?, regions = ?, use_all_blocks = ?
					 WHERE id = ?';
    db_query($query, array(
      $form_state['values']['title'],
      $form_state['values']['description'],
      $form_state['values']['file_name'],
      $form_state['values']['thumbnail_image'],
      $form_state['values']['default_layout'],
      $form_state['values']['regions'],
      $form_state['values']['use_all_blocks'],
      $form_state['values']['id']
        )
    );
  }
  else {
    $layout_id = db_insert('layout_manager_layouts')->fields(array(
          'title' => $form_state['values']['title'],
          'description' => $form_state['values']['description'],
          'file_name' => $form_state['values']['file_name'],
          'thumbnail_image' => $form_state['values']['thumbnail_image'],
          'default_layout' => $form_state['values']['default_layout'],
          'regions' => $form_state['values']['regions'],
          'use_all_blocks' => $form_state['values']['use_all_blocks']
        ))->execute();

    $form_state['values']['id'] = $layout_id;
  }

  // TODO verify this function wasn't doing anything
  // _layout_manager_save_with_types($form_state['values']);
  drupal_set_message(t('Saved layout %name.', array('%name' => $form_state['values']['title'])));
  watchdog('layout_manager', 'Saved layout %name.', array('%name' => $form_state['values']['title']), WATCHDOG_NOTICE, l(t('add'), 'admin/structure/layout_manager/'));
  $form_state['redirect'] = 'admin/structure/layout_manager';
  return;
}

function layout_manager_set_default_layout_submit($form, &$form_state) {
  list($layout, $regions) = _layout_manager_process_layout_options($_POST['regions_return']);
  $layout = _layout_manager_load_layout((int) $layout);
  $type = $form['content_type']['#value'];

  $sql = "DELETE FROM {layout_manager_layouts_default_blocks} WHERE content_type = ':type'";

  $res = db_query($sql, array(':type' => $type));

  if ($layout) {
    foreach ($regions as $region => $blocks) {
      foreach ($blocks as $bid => $order) {
        $sql = "INSERT INTO {layout_manager_layouts_default_blocks} (`bid` , `content_type` , `region`, `sort_order`) VALUES (':bid', ':type', ':region', ':order')";
        $res = db_query($sql, array(':bid' => $bid, ':type' => $type, ':region' => $region, ':order' => $order));
      }
    }
  }

  $sql = db_delete('layout_manager_layout_defaults')->condition('content_type', $type);
  $sql->execute();

  $sql = db_insert('layout_manager_layout_defaults')->fields(array('content_type' => $type, 'layout_id' => $layout->id));
  $sql->execute();

  drupal_set_message(t('Saved default layout for %name.', array('%name' => $type)));
  $form_state['redirect'] = 'admin/structure/layout_manager/defaults';
  return;
}
